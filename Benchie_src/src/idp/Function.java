package idp;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Function extends PFSymbol {

  private final Map<String, String> values;

  public Function(String name, Structure s) {
    super(name, s);
    values = new TreeMap<>();
  }

  public void add(String key, String value) {
    values.put(key, value);
  }

  @Override
  public String toString() {
    String result = name;
    result += "={";
    for (String v : values.keySet()) {
      result += "\"" + v + "\"->\"" + values.get(v) + "\";\n";
    }
    result += "}";
    return result;
  }

  public Set<String> domain() {
    return values.keySet();
  }

  public Collection<String> image() {
    return values.values();
  }

}
