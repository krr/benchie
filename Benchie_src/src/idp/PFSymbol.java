package idp;

public abstract class PFSymbol {

  public final String name;

  public PFSymbol(String name, Structure s) {
    this.name = name;
    s.add(this);
  }

}