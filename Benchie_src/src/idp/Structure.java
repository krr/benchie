package idp;

import java.util.ArrayList;
import java.util.List;

public class Structure {

  String name, voc;
  private final List<PFSymbol> symbols;

  public Structure(String name, String voc) {
    this.name = name;
    this.voc = voc;
    symbols = new ArrayList<>();
  }

  @Override
  public String toString() {
    String result = "structure " + name + ":" + voc + " {\n";
    for (PFSymbol f : symbols) {
      result += f.toString() + "\n";
    }
    result += "}";
    return result;
  }

  public void add(PFSymbol f) {
    symbols.add(f);
  }

}
