package idp;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

public class Type extends PFSymbol {

  private Set<String> values;

  public Type(String name, Structure s) {
    super(name, s);
    values = new TreeSet<>();
  }

  public Type(String name, Structure s, Collection<String> elements) {
    this(name, s);
    values.addAll(elements);
  }

  public void add(String s) {
    values.add(s);
  }
  
  public void addAll(Collection<String> strs) {
	values.addAll(strs);
  }

  @Override
  public String toString() {
    String result = name;
    result += "={\n";
    for (String v : values) {
      result += "\"" + v + "\";\n";
    }
    result += "}\n";
    return result;
  }
}
