package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Benchmark {

  // NOTE: no token should start with another token (eg "step" vs "steptype" is not allowed)
  public static final String PBTOKEN = "problemdir";
  public static final String BENCHTOKEN = "benchdir";
  public static final String IDPTOKEN = "idpdir";
  public static final String STEPTOKEN = "step";
  public static final String TYPETOKEN = "type";
  public static final String RESETTOKEN = "full_reset";

  // TODO: detect whether all data is given
  private final File config;
  public String problemDir, benchDir, idpDir; // TODO: make this final...
  public final String runCommand;
  public boolean reset;

  public Benchmark(File cfg, String runCommand) throws FileNotFoundException, IOException {
    config = cfg;
    problemDir = "";
    benchDir = "";
    idpDir = "";
    this.runCommand = runCommand;
    reset = false;

    Scanner scanner = new Scanner(config);
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      problemDir = Tools.parseTo(line, PBTOKEN, problemDir);
      benchDir = Tools.parseTo(line, BENCHTOKEN, benchDir);
      idpDir = Tools.parseTo(line, IDPTOKEN, idpDir);
      reset = Tools.parseTo(line, RESETTOKEN, reset);
    }
    scanner.close();

    File runComFile = new File(runCommand);
    if (!runComFile.exists()) {
      throw new IllegalArgumentException("The runCommand.sh file can not be found...");
    }
    if (problemDir.contains(getBenchDir())) {
      throw new IllegalArgumentException("To preserve data, benchmark directory cannot contain problem directory.");
    }

    initializeResultsDir();
  }

  public String getDataDir() {
    return benchDir + "/_Benchie";
  }

  public String getBenchDir() {
    return benchDir;
  }

  private void initializeResultsDir() throws IOException {
    File benchDirFile = new File(getBenchDir());
    File probDir = new File(problemDir);

    if (benchDirFile.exists() && reset) {
      System.out.println("Deleting " + benchDirFile.getAbsolutePath());
      Tools.delete(benchDirFile);
    }

    System.out.println("Creating " + benchDirFile.getAbsolutePath());
    // copy directory structure
    List<File> folders = Tools.getFoldersRecursively(probDir);
    for (File dir : folders) {
      String newName = dir.getAbsolutePath().replaceFirst(problemDir, getDataDir());
      File newDir = new File(newName);
      newDir.mkdirs();
    }
    // create soft links to files
    List<File> files = Tools.getFilesRecursively(probDir);
    for (File dir : files) {
      String oldName = dir.getAbsolutePath();
      String newName = oldName.replaceFirst(problemDir, getDataDir());
      File newFile = new File(newName);
      if (!newFile.exists()) {
        Files.createSymbolicLink(Paths.get(newName), Paths.get(dir.getAbsolutePath()));
      }
    }
  }

  public void run() throws FileNotFoundException {
    Scanner scanner = new Scanner(config);
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      String stepName = "";
      stepName = Tools.parseTo(line, STEPTOKEN, stepName);
      if (!stepName.isEmpty()) {
        // scanning for stepinfo
        String type = "";
        ArrayList<String> stepInfo = new ArrayList<>();
        while (scanner.hasNextLine()) {
          line = scanner.nextLine();
          if (line.isEmpty()) {
            break;
          }
          stepInfo.add(line);
          type = Tools.parseTo(line, TYPETOKEN, type);
        }

        Step next;
        if (type.equalsIgnoreCase("idp")) { // TODO: refactor selection to case when needed
          next = new IDPStep(this, stepName, stepInfo);
        } else {
          next = new TxtStep(this, stepName, stepInfo);
        }
        
        // make necessary /step directories
        File runs = new File(next.getRunsDir());
        runs.mkdirs();

        next.run();
      }
    }
    scanner.close();

    try {
      Thread.sleep(100); // TODO: Allow time for IO to finish? (don't think
      // needed, so low value)
    } catch (InterruptedException e) {
      System.out.println(e);
    }
  }

}
