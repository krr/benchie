package main;

import idp.Structure;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class IDPStep extends Step {

  public IDPStep(Benchmark b, String name, ArrayList<String> info) {
    super(b, name, info);
    if (benchmark.idpDir == null || benchmark.idpDir.isEmpty()) {
      throw new IllegalArgumentException("No IDP step can be made without a given IDP location!");
    }
  }

  private String getStructureName() {
    return input + ".struc";
  }

  private String getRunsName() {
    return input + ".runs";
  }

  private String parseIDPRun(String idprun) {
    idprun = idprun.replace('{', ' ');
    idprun = idprun.replace('}', ' ');
    // remove all whitespace
    idprun = idprun.replaceAll("\\s", "");
    idprun = idprun.replaceAll("\"", "");

    idprun = idprun.replace(';', '\n');
    idprun = idprun.replace(',', ' ');
    return idprun;
  }

  @Override
  protected File getRuns() throws FileNotFoundException {
    // general idea: use IDP to generate a runs file. Write the file, and return it.
    Structure struc = Tools.readStructure(new File(benchmark.getDataDir()));

    PrintWriter structureWriter = new PrintWriter(getStructureName());
    structureWriter.print(struc.toString());
    structureWriter.close();

    ProcessBuilder pb = new ProcessBuilder(benchmark.idpDir,
            input, getStructureName());

    File runsFile = new File(getRunsName());
    PrintWriter runsWriter = new PrintWriter(runsFile);
    try {
      String idpquery;
      Process p = pb.start();
      BufferedReader outReader = new BufferedReader(
              new InputStreamReader(p.getInputStream()));
      BufferedReader errorReader = new BufferedReader(
              new InputStreamReader(p.getErrorStream()));

      while ((idpquery = outReader.readLine()) != null) {
        runsWriter.print(parseIDPRun(idpquery));
      }
      runsWriter.close();

      String error = "";
      while ((idpquery = errorReader.readLine()) != null) {
        error += idpquery + "\n";
      }
      p.waitFor();
      if (!error.equals("")) {
        System.out.println("IDP has the following errorstream: ");
        System.out.println(error);
      }

    } catch (IOException | InterruptedException e) {
      System.out.println(e);
    }
    return runsFile;
  }

}
