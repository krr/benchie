package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLDecoder;

// TODO: fix try-catch blocks, they should be as small as possible
public class Main {

  public static void main(String[] args) throws FileNotFoundException,
          IOException {

    System.out.println("Welcome to Benchie :)");

    File configFile;
    if (args.length == 1) {
      configFile = new File(args[0]);
    } else {
      System.err
              .println("Benchie requires a path to a config file as argument.");
      return;
    }

    String path = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
    String decodedPath = URLDecoder.decode(path, "UTF-8");
    decodedPath = decodedPath.substring(0, decodedPath.lastIndexOf("/") + 1) + "run_command.sh";
    System.out.println("Looking for run_command.sh at " + decodedPath);

    Benchmark b = new Benchmark(configFile, decodedPath);
    b.run();

    System.out.println("Benchie finished :)");
  }

}
