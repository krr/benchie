/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author jodv
 */
public class Run implements Runnable {

  private final String command;
  private final Step step;
  private String uniqueName;

  public Run(String c, Step s) {
    command = c;
    step = s;
  }

  public String getUniqueName() {
    if (uniqueName == null) {
      String result = "";
      String[] noBlanks = command.trim().split("\\s+"); // splits string based on one or more spaces
      for (String s : noBlanks) {
        result += s.replaceAll(".*/", "") + "+"; // removes initial paths
      }
      String hashcode = Integer.toHexString(command.hashCode());
      while(hashcode.length()<8){
        hashcode = "0"+hashcode; // hash code should have fixed length of 8
      }
      uniqueName = result + hashcode; // adds hash code to distinguish in case of different paths
    }
    return uniqueName;
  }

  public String getRsrcPath() {
    return step.getRunsDir() + "/" + getUniqueName();
  }

  @Override
  public void run() {
    String rsrcPath = getRsrcPath();
    File rsrcFile = new File(rsrcPath);
    if (rsrcFile.exists()) {
      System.out.println("Skipping " + getUniqueName());
      try {
        step.notifyRunFinished(this);
      } catch (Exception ex) {
        System.out.println("Error occurred. Description: " + ex.getMessage());
      }
      return;
    }

    System.out.println("Running " + getUniqueName());
    ProcessBuilder pb = new ProcessBuilder(step.benchmark.runCommand, Integer.toString(step.timelim), Integer.toString(step.memlim),
            command, rsrcPath);
    pb.redirectErrorStream(true);

    try {
      final Process p = pb.start();
      p.waitFor();
      step.notifyRunFinished(this);
    } catch (Exception e) {
      System.out.println("Error occurred. Description: " + e.getMessage());
    }
  }

}
