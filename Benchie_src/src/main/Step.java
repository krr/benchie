package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class Step {

  public static final String TIMETOKEN = "timelim";
  public static final String MEMTOKEN = "memlim";
  public static final String THREADTOKEN = "threads";
  public static final String INPUTTOKEN = "input";
  public static final String EXTRACTTOKEN = "extract";
  public static final String OUTCODETOKEN = "Command exited with";
  public static final String CPUTOKEN = "cpu time:";
  public static final String MAXTOKEN = "max memory:";
  private static final String CSVHEADER = "Name,OutCode,Time,Mem";

  public int timelim, memlim, threads; // TODO make final
  public String name, input, extract;
  public Benchmark benchmark;
  protected SortedSet<String> csvLines;
  protected PrintWriter csvWriter;

  public Step(Benchmark b, String name, ArrayList<String> info) {
    this.name = name;
    benchmark = b;
    input = "";
    extract = "";
    for (String line : info) {
      timelim = Tools.parseTo(line, TIMETOKEN, timelim);
      memlim = Tools.parseTo(line, MEMTOKEN, memlim);
      threads = Tools.parseTo(line, THREADTOKEN, threads);
      input = Tools.parseTo(line, INPUTTOKEN, input);
      extract = Tools.parseTo(line, EXTRACTTOKEN, extract);
    }
    csvLines = new TreeSet<>();
  }

  public String getRunsDir() {
    return benchmark.getBenchDir() + "/" + name;
  }

  public String getCSVPath() {
    return benchmark.getBenchDir() + "/" + name + ".csv";
  }

  private String getCsvLine(Run run) throws FileNotFoundException {
    File rsrcFile = new File(run.getRsrcPath());

    // "Name;Time;Mem;OutCode"
    String result = run.getUniqueName();

    Scanner scanner = new Scanner(rsrcFile);
    if(!scanner.hasNextLine()) {
      throw new RuntimeException("File for " + result + " was empty.");
    }
    int outCode = 0; // if nothing happened, no outcode is given, so it must be 0
    double timeUsed = -1.0;
    int memUsed = -1;
    /**
     * Example rsrc file: " Command exited with non-zero status 137 command:
     * /home/jodv/local/workspace/idp2mip/get_mps.sh
     * /home/jodv/local/workspace/idp2mip/benchdata/chromatic_number/chromatic_number
     * /home/jodv/local/workspace/idp2mip/benchdata/chromatic_number/instances/le450_15a.col.struc
     * cpu time: 2.82 max memory: 1367504 ----------OUTPUT----------
     * <outputlines>
     * "
     * NOTE: The first line is only present when a non-zero status is returned.
     */
    // read outcode
    String line = scanner.nextLine();
    if (line.startsWith(OUTCODETOKEN)) {
      String[] subs = line.split(" ");
      outCode = Integer.parseInt(subs[subs.length - 1]);
      line = scanner.nextLine();
    }
    result += "," + outCode;
    // skip command line
    // read cpu time used
    timeUsed = Tools.parseTo(scanner.nextLine(), CPUTOKEN, timeUsed);
    result += "," + timeUsed;
    // read max memory used
    memUsed = Tools.parseTo(scanner.nextLine(), MAXTOKEN, memUsed);
    result += "," + memUsed;
    // skip "---OUTPUT---"
    scanner.nextLine();
    while (scanner.hasNextLine() && extract.equalsIgnoreCase("true")) {
      result += "," + scanner.nextLine();
    }
    scanner.close();

    return result;
  }

  protected abstract File getRuns() throws FileNotFoundException;

  public void run() throws FileNotFoundException {
    System.out.println("Initializing step: " + name);
    File runFile = getRuns();
    System.out.println("Reading runs: " + runFile.getAbsolutePath());

    ArrayList<Run> allRuns = new ArrayList<>();
    Scanner scanner = new Scanner(runFile);
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      allRuns.add(new Run(line, this));
    }

    csvWriter = new PrintWriter(getCSVPath());
    csvWriter.println(CSVHEADER);

    ExecutorService executorPool = Executors.newFixedThreadPool(threads);
    for (Run r : allRuns) {
      executorPool.execute(r);
    }

    executorPool.shutdown();

    while (!executorPool.isTerminated()) {
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        System.out.println("Problem with multithreading detected: " + e.getMessage());
      }
    }
    csvWriter.close();

    csvWriter = new PrintWriter(getCSVPath());
    csvWriter.println(CSVHEADER);
    for (String line : csvLines) {
      csvWriter.println(line);
    }
    csvWriter.close();
  }

  public synchronized void notifyRunFinished(Run r) throws FileNotFoundException {
    String csvline="";
    try{
      csvline = getCsvLine(r);
    } catch (FileNotFoundException e) {
      try{
        Thread.sleep(1000); // maybe the OS was not ready yet with creating the file.
      } catch (InterruptedException e2) { /*ignore*/ }
      try{
        csvline = getCsvLine(r);
      } catch (FileNotFoundException e3){
        System.out.println("Could not read output file for " + r.getUniqueName() + "\n " + e3.getMessage());
        System.out.println("Check whether this file exists, and re-run Benchie.");
      }
    }
    csvLines.add(csvline);
    csvWriter.println(csvline);
    csvWriter.flush();
  }

}
