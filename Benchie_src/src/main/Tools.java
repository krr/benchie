package main;

import idp.Function;
import idp.Structure;
import idp.Type;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Tools {

  public static String parseTo(String line, String token, String variable) {
    if (line.startsWith(token)) {
      return line.replaceFirst(token, "").trim();
    } else {
      return variable;
    }
  }

  public static int parseTo(String line, String token, int variable) {
    if (line.startsWith(token)) {
      return Integer.parseInt(line.replaceFirst(token, "").trim());
    } else {
      return variable;
    }
  }

  public static double parseTo(String line, String token, double variable) {
    if (line.startsWith(token)) {
      return Double.parseDouble(line.replaceFirst(token, "").trim());
    } else {
      return variable;
    }
  }

  public static boolean parseTo(String line, String token, boolean variable) {
    if (line.startsWith(token)) {
      return Integer.parseInt(line.replaceFirst(token, "").trim()) != 0;
    } else {
      return variable;
    }
  }

  private static boolean isSystemFile(File f) {
    return f.isHidden() || f.getName().endsWith("~") || f.getName().startsWith(".");
  }

  /* @param isFile
   *            asserts whether the result contains only files or only
   *            directories
   */
  public static List<File> getFiles(File dir, boolean isFile) {
    assert (!dir.isFile());
    File[] fileArray = dir.listFiles();
    List<File> result = new ArrayList<>();
    if (fileArray == null) {
      benchieError("Nullpointer problem.\nCould not list contents of\n" + dir.getAbsolutePath());
    }
    for (File f : fileArray) {
      if (f.isDirectory() != isFile && !isSystemFile(f)) {
        result.add(f);
      }
    }
    Collections.sort(result);
    return result;
  }

  // only returns files, not folders
  public static List<File> getFilesRecursively(File dir) {
    List<File> dirs = getFoldersRecursively(dir);
    List<File> result = new ArrayList<>();
    for (File f : dirs) {
      result.addAll(getFiles(f, true));
    }
    return result;
  }

  // only returns folders, not files
  public static List<File> getFoldersRecursively(File dir) {
    List<File> result = new ArrayList<>();
    result.add(dir);
    List<File> dirs = getFiles(dir, false);
    for (File f : dirs) {
      result.addAll(getFoldersRecursively(f));
    }
    return result;
  }

  public static Structure readStructure(File location) {
    assert (location.isDirectory());
    List<File> objects = new ArrayList<>();

    objects.addAll(getFoldersRecursively(location));
    objects.addAll(getFilesRecursively(location));

    Structure struc = new Structure("BenchieStruc", "BenchieVoc");

    Function name = new Function("GetName", struc);
    for (File f : objects) {
      name.add(f.getAbsolutePath(), f.getName());
    }
    @SuppressWarnings("unused")
    Type names = new Type("Name", struc, name.image());

    Function parent = new Function("Parent", struc);
    for (File f : objects) {
      if (f.equals(location)) {
        parent.add(f.getAbsolutePath(), f.getAbsolutePath());
      } else {
        parent.add(f.getAbsolutePath(), f.getParentFile()
                .getAbsolutePath());
      }
    }

    Function purename = new Function("GetPureName", struc);
    Function extension = new Function("Extension", struc);
    for (File f : objects) {
      if (f.isFile()) {
        ArrayList<String> splitName = splitExtension(f);
        purename.add(f.getAbsolutePath(), splitName.get(0));
        extension.add(f.getAbsolutePath(), splitName.get(1));
      }
    }
    names.addAll(purename.image());
    @SuppressWarnings("unused")
    Type ext = new Type("Ext", struc, extension.image());

    Type file = new Type("File", struc);
    Type dir = new Type("Dir", struc);
    Type object = new Type("Object", struc);
    for (File f : objects) {
      if (f.isFile()) {
        file.add(f.getAbsolutePath());
      } else {
        dir.add(f.getAbsolutePath());
      }
      object.add(f.getAbsolutePath());
    }

    return struc;
  }

  public static ArrayList<String> splitExtension(File in) {
    String[] parts = in.getName().split("\\.");
    assert (parts.length > 0);
    String purename = parts[0];
    String extension;
    if (parts.length == 1) {
      extension = "EMPTY";
    } else {
      for (int i = 1; i < parts.length - 1; ++i) {
        purename += "." + parts[i];
      }
      extension = parts[parts.length - 1];
    }
    ArrayList<String> result = new ArrayList<>();
    result.add(purename);
    result.add(extension);
    return result;
  }

  public static void delete(File f) throws IOException {
    if (f.isDirectory()) {
      for (File c : f.listFiles()) {
        delete(c);
      }
    }
    if (!f.delete()) {
      throw new FileNotFoundException("Failed to delete file: " + f);
    }
  }

  public static void benchieError(String message) {
    System.err.println("Benchie had the following error:");
    System.err.println(message);
    System.exit(10);
  }
}
