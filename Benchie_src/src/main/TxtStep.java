package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class TxtStep extends Step {

  public TxtStep(Benchmark b, String name, ArrayList<String> info) {
    super(b, name, info);
  }

  @Override
  protected File getRuns() throws FileNotFoundException {
    return new File(input);
  }

}
