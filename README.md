Benchie
=======

Automated benchmarking system.  
Memory limit is in MiB, time limit is in seconds.

Binary
------
You can find the compiled Benchie.jar binary in the [downloads](https://bitbucket.org/krr/benchie/downloads) section.

Outcodes
--------
0 means everything went great!  
137 means the benchmark reached a timeout.  
1 or 134 typically means the benchmark reached the memory limit.  

NOTE: in case of using LUA printouts, put "io.stdout:setvbuf("no")" at the start of your procedure to avoid data staying in a buffer after the program has been forced to shutdown.

Example
-------

    problemdir /home/jodv/local/workspace/idp2mip/problems
    benchdir /home/jodv/local/workspace/idp2mip/benchdata
    idpdir /home/jodv/local/workspace/idp_install/bin/idp
    full_reset 1

    step idp
    type txt
    timelim 200
    memlim 2000
    threads 4
    input /home/jodv/local/workspace/idp2mip/idp.idp.runs
    extract true

    step get_mps
    type txt
    timelim 200
    memlim 2000
    threads 4
    input /home/jodv/local/workspace/idp2mip/get_mps.idp.runs

    step cplex
    type idp
    timelim 200
    memlim 2000
    threads 4
    input /home/jodv/local/workspace/idp2mip/cplex.idp
    extract true
    
IDP speedtests
--------------
See folder <idp_speedtest>. The Benchie configuration file is called "1_IDP.benchie", so running "java -jar ../Benchie.jar 1_IDP.benchie" in the <idp_speedtest> folder will run the benchmarks.