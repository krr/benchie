#!/bin/bash

#inputs are paths.
instances_asp=$1 #NOTE: must be a full path
instances_factlist=$2 #NOTE: can be a relative path

cd $instances_factlist

for f in $instances_asp/*
do
	filename=$(basename $f .asp)
	filename="$filename.fctlst"
	echo "Processing $filename file..."
	echo "factlist S:FactListVoc{" > $filename
	cat $f >> $filename
	echo "}" >> $filename
done
