#!/bin/bash

# Input file: the file containing the .csv data you wish to transform into the IDP file
inputfile="data.csv"
# Output file: the output file that will contain the IDP file for the given .csv data
outputfile="data.idp"

# ROW_DELIMITER: the delimiter used in the .csv input file
ROW_DELIMITER=","

# nonint_columns: the columns in the input .csv file that do not represent integer values (starting from 0)
nonint_columns="0 1 2" 

# NOTE: 
# to quickly parse the resulting .idp file, one should add 
#   stdoptions.assumeconsistentinput = true
# to the install_location/var/config.idp file of your IDP build



##########################################
# Do not modify anything below this line #
##########################################

# Static part of IDP file
echo "vocabulary V {
  type columnNameType isa string
  type colIndex
  columnName(colIndex):columnNameType
  columnIndex(columnNameType):colIndex
  
  type rowIndex
  type dataValues
  val(rowIndex,colIndex):dataValues
}

structure S : V {" > $outputfile

while IFS="$ROW_DELIMITER" read -ra ADDR; do
    COLS="${ADDR[@]}"
done <<< "$(head -n 1 $inputfile)" # Only use first line



# # # # SORT INTERPRETATIONS
# # sort columnNameType
# echo "  columnNameType = { " >> $outputfile
# for i in $COLS; do
#     echo "    \"$i\";" >> $outputfile
# done
# echo "  }" >> $outputfile   # end of sort columnNameType interpretation
# 
# # sort colIndex
# let maxCol=${#COLS[@]}-1
# echo "  colIndex = { 0..$maxCol }" >> $outputfile
# 
# # sort rowIndex
# nr_of_lines_in_input_file=$(wc -l < "$inputfile")
# let maxRow=${nr_of_lines_in_input_file}-1
# echo "  rowIndex = { 1..$maxRow }" >> $outputfile



# # # FUNCTION INTERPRETATIONS
# Column indexes (COLUMNINDEX => COLUMN)
echo "  columnName = { " >> $outputfile
index=0
for i in $COLS; do
    echo "    $index->\"$i\";" >> $outputfile
    let index=$index+1
done
echo "  }" >> $outputfile   # end of columnName/2 interpretation


# Column indexes (COLUMN => COLUMNINDEX)
echo "  columnIndex = { " >> $outputfile
index=0
for i in $COLS; do
    echo "    \"$i\"->$index;" >> $outputfile
    let index=$index+1
done
echo "  }" >> $outputfile   # end of columnIndex/2 interpretation


# Row values (ROWINDEX,COLUMNINDEX => VALUE)
echo "  val = {" >> $outputfile
rowindex=1
while IFS="$ROW_DELIMITER" read -ra ADDR; do
    colindex=0
    echo "Doing row: " $rowindex
    for i in "${ADDR[@]}"; do
# 	Depending on whether the column is a nonint, fill in the value
	if [[ " ${nonint_columns[*]} " == *" $colindex "* ]]; then
	    echo "    $rowindex,$colindex->\"$i\"; " >> $outputfile
	else
	    echo "    $rowindex,$colindex->$i; " >> $outputfile
	fi
	let colindex=$colindex+1
    done
    let rowindex=$rowindex+1
#     echo "done line"
done <<< "$(tail -n +2 $inputfile)" # Only use line 2 and so on
echo "  }" >> $outputfile # end of val/3 interpretation

echo "}" >> $outputfile # end of structure

cat $outputfile