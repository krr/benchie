output_file="data.csv"

DEFAULT_VALUE="0"
SUBFOLDERS="./"
# SUBFOLDERS="selected_instances_results/"
SUFFIXES=".groundadoff .groundadcheap .groundnaive .groundadcomplete .groundadall"
SUFFXESASARRAY=($SUFFIXES)


# Create header for .csv file
echo -n "subfolder,problemName,problemInstance" > $output_file
for suffix in $SUFFIXES
do
  echo -n ",memSize$suffix(b)" >> $output_file
  echo -n ",time$suffix(s)" >> $output_file
  echo -n ",bash_return_code$suffix" >> $output_file
  echo -n ",lua_return_code$suffix" >> $output_file
  echo -n ",preground_time$suffix(ms)" >> $output_file
  echo -n ",ground_size$suffix" >> $output_file
done
echo "" >> $output_file  # End this row in the data file



# The below function searches for first number in the piped argument and prints 
# it to the $output_file
function fetchNrFromLineContaining(){
  grep "$1" "$2" | sed -r 's|^([^.]+).*$|\1|; s|^[^0-9]*([0-9]+).*$|\1|' | awk 'BEGIN {max = 0} {if ($1>max) max=$1} END {print max}' | tr "\\n" "," >> $output_file
}

for subfolder in $SUBFOLDERS
do
  echo -en "Doing subfolder: " $subfolder "\n"

  # Find file for first suffix, based on this file, handle all suffixes
  # In this way, each file has only one row with a set of columns for each suffix
  for file in `find $subfolder -type f -name "*${SUFFXESASARRAY[0]}" -print`
  do
      echo -n $subfolder"," >> $output_file
      dirname=`dirname $file`;
      echo -n `basename $dirname`"," >> $output_file
      fileNoExtension="${file%.*}"
#       echo -n `basename $fileNoExtension`"," >> $output_file
      echo -n `basename ${fileNoExtension##*+}`"," >> $output_file
    for suffix in $SUFFIXES
    do
      fileForThisSuffix=$fileNoExtension$suffix
      echo "doing file" $fileForThisSuffix
      runstatsFileName=$fileForThisSuffix.TIM
      fetchNrFromLineContaining "max memory" $runstatsFileName
      fetchNrFromLineContaining "cpu time" $runstatsFileName
      fetchNrFromLineContaining "Command exited with non-zero status" $runstatsFileName
      fetchNrFromLineContaining "RETURNCODE" $fileForThisSuffix
      fetchNrFromLineContaining "grounding-start" $fileForThisSuffix
      fetchNrFromLineContaining "groundsize" $fileForThisSuffix
    done
    echo "" >> $output_file  # End this row in the data file
  done
done



## little explanation to the long grep lines: tail -1 selecteert de laatste lijn van alle matchende, sed -r etc selecteert het eerst matchende getal
