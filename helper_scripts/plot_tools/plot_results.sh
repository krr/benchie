#!/bin/bash

mkdir images

DEFAULT_COL=2
col=${1-$DEFAULT_COL}
DEFAULT_TITLE="Time (s)"
title=${2-$DEFAULT_TITLE}

for problem in $(cat .problems.txt)
do
  COLOR=1
  OUTPUT_FILE=images/$problem.img

  PLOTS='set term png
  \nset title "Plot of '$title' of '$problem' instances"
  \nset xlabel "problem instances"
  \nset ylabel "'$title'"
  \nset output "'$OUTPUT_FILE'"\nplot '
  for phase in $(cat .phases.txt)
  do
    filename="out/"$problem.$phase
    echo $filename
# #     echo -e $PRE"\nplot '$filename' using ("'$1'"*$nrProblems+"'$2'"):3 notitle"
    PLOTS=$PLOTS'"'$filename'" using 0:'$col' title "'$phase'" ls '$COLOR', '
    let COLOR=$COLOR+1
  done
  
  let PLOTSMIN1=${#PLOTS}-2
  PLOTS=${PLOTS:0:$PLOTSMIN1}
  echo -e $PLOTS > ./plotresults.gnuplot
  gnuplot plotresults.gnuplot
  gwenview $OUTPUT_FILE & > /dev/null
done

rm plotresults.gnuplot
