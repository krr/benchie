#!/bin/bash

> .problems.txt
> .phases.txt

mkdir out

# inputdir=data
# fileName=$1
for fileName in $@
do

  echo "Processing "$fileName"..."
  let newlen=${#fileName}-4
  newname=${fileName:0:newlen}
  newname=${newname//\-/}
  newname=${newname//[0-9]/}
  newname=`basename $newname`
  echo $newname >> .problems.txt
  tail -n +2 $fileName > /tmp/new_in.tmp
  
##############################################
# Sort so order of problem instances is fixed
  for line in $(cat /tmp/new_in.tmp)
  do
      echo "$line"
  done | sort -u > /tmp/$newname

##############################################
# Extract all phases in a file (.phases.txt) so that it is listed
# 
# phase1
# phase2
# phase3
# ...
#

  cp /tmp/$newname /tmp/phases_in.tmp
  sed -i -r 's/^([^;]*)\;(.*)$/\1/g' /tmp/phases_in.tmp
#   cat .phases.txt
  for line in $(cat /tmp/phases_in.tmp)
  do
      echo "$line"
  done | sort -u > .phases.txt
#   cat .phases.txt

##############################################
# Now process the file, extract each time and memory (and outcode?) for each phase into a seperate file
  for phase in $(cat .phases.txt)
  do
    cp /tmp/$newname /tmp/tmpfile.tmp
    outfile=out/$newname"."$phase
    echo "#Problem_Instance    Time    Memory    Outcode" > $outfile
    for line in $(cat /tmp/tmpfile.tmp)
    do
      let phaselen=${#phase}+1
      lineInit=${line:0:phaselen}
      # If the line init fits the phase, this is a line you have to use as the current phase
      if [ $phase";" = $lineInit ];
      then
	# handle line, filter out data and append to data
	echo $line > /tmp/tmpline.tmp
	sed -i -r 's/^.*\;(\S*)\;(\S*)\;(\S*)\;(\S*)$/\1    \2    \3    \4/g' /tmp/tmpline.tmp
	sed -i -r 's/(\-|\_|\+)//g' /tmp/tmpline.tmp
	cat /tmp/tmpline.tmp >> $outfile
      fi
    done
    rm /tmp/tmpline.tmp
  done
  rm /tmp/tmpfile.tmp
done
