#!/bin/bash

directory=/tmp/speedtest

for f in $directory/*; do
	echo -n $(basename "$f")
	echo -n ";"
	echo -n `stat -c %Y $f`
	echo -n ";"
	echo -n `awk -F ';' '{s+=$3} END {print s}' $f`
	echo -n ";"
	echo -n `wc -l < $f`
	echo ""
done
