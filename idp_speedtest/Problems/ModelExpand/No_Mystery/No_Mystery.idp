/**
* Solving the NoMystery planning problem
*/

// NOTE: all instances have only one truck

vocabulary FactListVoc{
    type Object
    type Truck isa Object
    type Package isa Object
    type Location
    type Step isa int
    type Fuel isa int
    type Cost isa int

    fuelcost(Cost,Location,Location)
    step(Step)
    at(Object,Location)
    fuel(Truck,Fuel)
    //NOTE: if some package has no goal, it will not belong to the type "package".
    //This does not cause problems since these packages are completely irrelevant
    goal(Package,Location)
}

vocabulary V{
    extern vocabulary FactListVoc

    type Action constructed from {Drive, PickUp, DropOff, Nothing}

    // Given:  
    TruckStart:Location
    FuelStart:Fuel  

    // Find:
    Load(Package,Step)
    Unload(Package,Step)
    DriveTo(Location,Step)

    // Using:
    Connected(Location,Location)
    TruckAt(Step):Location
    Loaded(Package,Step)
    AtGoalLocation(Package,Step)
    ActionAt(Step):Action
    Loadable(Package,Step)
    Unloadable(Package,Step)
    Blocked(Location,Step)
}

theory T:V{
    {
        TruckStart=l <- ?t: Truck(t) & at(t,l).
    }
    {
        FuelStart=f <- ?t: fuel(t,f).
    }

    {
        Connected(l1,l2) <- ?c: fuelcost(c,l1,l2).
    }
    {
        TruckAt(MIN[:Step])=TruckStart.
        TruckAt(step+1) = l <- TruckAt(step) = l & ActionAt(step) ~= Drive.
        TruckAt(step+1) = l <- DriveTo(l,step).
    }
    {
        !p[Package]: AtGoalLocation(p,MIN[:Step]) <- ?l: goal(p,l) & at(p,l).
        AtGoalLocation(p,s+1) <- AtGoalLocation(p,s). //Once at goal, do not move it.
        AtGoalLocation(p,s+1) <- Unload(p,s). //You can only reach goal by being unloaded onto goal. The condition that Truck is at right location for unloading comes later
    }
    {
        Loaded(p,s+1) <- Load(p, s).
        Loaded(p,s+1) <- Loaded(p,s) & ~Unload(p,s).
    }
    {
        !p[Package] s: Loadable(p,s) <- ~AtGoalLocation(p,s) & ~Loaded(p,s) & at(p,TruckAt(s)).
        Unloadable(p,s) <- goal(p,TruckAt(s)) & Loaded(p,s).
    }

    //Goal constraint
    ! p: AtGoalLocation(p,MAX[:Step]).

    // Constrain actions
    //* Add the link between ActionAt and the various predicates
    //NOTE: You cannot use ?1 here!!! Important difference
    {
        !step: ActionAt(step)=Drive <- ? loc: DriveTo(loc,step).
        !step: ActionAt(step)=PickUp <- ? package: Load(package,step).
        !step: ActionAt(step)=DropOff <- ? package: Unload(package,step).
        !step: ActionAt(step)=Nothing <- ! p: AtGoalLocation(p,step).
    }

    // The last moment, you cannot do an action
    ActionAt(MAX[:Step])=Nothing.

    //*Partial function constraints for DriveTo, Load, Unload
    !step: #{loc : DriveTo(loc,step)} =< 1.
    !step: #{package : Load(package,step)} =< 1.
    !step: #{package : Unload(package,step)} =< 1.

    // The truck can only Load a package which is at the same location at the same time
    //Note: location of a package is either startlocation or goallocation. Packages get never dropped on wrong places!
    !s p: Load(p,s) => Loadable(p,s).

    // The truck can only Unload a package to the location the truck is in, and the package must be in the truck
    // And it is only allowed to unload packages to their goallocation
    //TODO: maybe there will be a speedup if we split this in two constraints (depending on bounds...
    !s p: Unload(p,s) => Unloadable(p,s).


    // Limit fuel usage
    // TODO Only correct if we have the guarantee that there is only one cost per edge.
    FuelStart >= sum{c loc step: DriveTo(loc,step) & fuelcost(c,TruckAt(step),loc): c }.

    // The truck can only drive to a location from a connected location.
    !step loc: DriveTo(loc,step) => Connected(TruckAt(step),loc). 

    // Superfluous constraints:
    // Note: truck may visit a location twice (if it is on the road to another location)

    // never drive back to where one comes from
    // can be extended: extra predicate Blocked(Location,Step)
    //TODO check performance
    {
        Blocked(l,s+1) <- TruckAt(s) = l.
        Blocked(l,s+1) <- Blocked(l,s) & ActionAt(s) = Drive.
    }
    !step loc: Blocked(loc,step) => ~DriveTo(loc,step).
    //!step: (?loc: DriveTo(loc,step) & Step(step+1)) => ~DriveTo(TruckAt(step),step+1).

    // A truck must load a package if possible
    !step: (? package: Loadable(package,step)) => ActionAt(step)=PickUp.

    // A truck must unload a package if possible AND if no packages can be loaded
    !step: (ActionAt(step)~=PickUp & ? package: Unloadable(package,step)) => ActionAt(step)=DropOff.

    // Steps where no action happens can only happen at the end
    !step: (Step(step+1) & ActionAt(step)=Nothing) => ActionAt(step+1)=Nothing.
}

//term TotalDistance:problemVoc{
//  sum{c loc step: DriveTo(loc,step) & fuelcost(c,TruckAt(step),loc): c }
//} 

procedure setOptions(){
    stdoptions.cpsupport=true
    stdoptions.xsb=true
}

procedure getStructure(){
    setOptions()
    setvocabulary(S,V)
    return S
}

procedure getTheory(){
    return T
}

structure Small : V {
    Cost = { 10..10 }
    Fuel = { 56..56 }
    Location = { "a"; "b" }
    Object = { "p0"; "t0" }
    Package = { "p0" }
    Step = { 1..5 }
    Truck = { "t0" }
    at = { "p0","a"; "t0","a" }
    fuel = { "t0",56 }
    fuelcost = { 10,"a","b"; 10,"b","a" }
    goal = { "p0","b" }
    step = { 1;2;3;4;5 }
}

procedure main(){
    setOptions()
    local sol = modelexpand(T,Small)[1]
    print(sol)
}
