#!/bin/bash

instance=$2
idp=idp
idpcommands=idp_commands.idp

$idp $idpcommands $1 $instance -e "getModel()" | grep "nbModels=" | awk -F[[:blank:]] '{print $2}' | tr -d ' '
exit ${PIPESTATUS[0]}
