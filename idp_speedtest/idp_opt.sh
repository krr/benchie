#!/bin/bash

instance=$2
idp=idp
idpcommands=idp_commands.idp

$idp $idpcommands $1 $instance -e "getOptimalModel()" 2>&1 | grep "bestvalue&&" | sed 's/[^0-9]//g' #| tr -d ' '
exit ${PIPESTATUS[0]}
